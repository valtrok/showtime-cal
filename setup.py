import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="showtime-cal",
    version="1.0",
    author="Corentin CAM",
    author_email="me@corentin.cam",
    description="Theater showtimes iCal generator",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/valtrok/showtime-cal",
    packages=setuptools.find_packages(),
    project_urls={"Source Code": "https://gitlab.com/valtrok/showtime-cal"},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    install_requires=['Flask', 'requests', 'icalendar'])
