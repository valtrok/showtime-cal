FROM python:alpine as builder

WORKDIR /usr/src/app

RUN pip install gunicorn

EXPOSE 5000

COPY . .

RUN pip install .

CMD [ "gunicorn", "showtime-cal:app", "-w", "1", "--threads", "5", "-b", "0.0.0.0:5000" ]
