from flask import Flask
from markupsafe import escape
from datetime import date, datetime, timedelta
import json
import requests
import icalendar

app = Flask(__name__)


@app.route("/<string:theater_id>.ical")
def showtime_cal(theater_id):
    movies = []
    today = date.today()
    for i in range(14):
        day = (today + timedelta(days=i)).strftime("%Y-%m-%d")
        movies.extend(
            json.loads(
                requests.post(
                    f"https://www.allocine.fr/_/showtimes/theater-{escape(theater_id)}/d-{day}/",
                    data={
                        "filters": []
                    }).text)["results"])

    cal = icalendar.Calendar()
    for movie in movies:
        if isinstance(movie["movie"]["runtime"], int):
            run_time = timedelta(seconds=movie["movie"]["runtime"])
        else:
            run_time = timedelta(
                hours=int(movie["movie"]["runtime"].split("h")[0]),
                minutes=int(
                    movie["movie"]["runtime"].split("h")[1].strip().split("m")[0]))
        for lang in ["multiple", "original"]:
            for showtime in movie["showtimes"][lang]:
                event = icalendar.Event()
                start_time = datetime.strptime(showtime["startsAt"],
                                               "%Y-%m-%dT%X")
                event["dtstart"] = datetime.strftime(start_time,
                                                     "%Y%m%dT%H%M%S")
                event["dtend"] = datetime.strftime(start_time + run_time,
                                                   "%Y%m%dT%H%M%S")
                event["summary"] = movie["movie"]["title"] + (
                    " [VO]" if lang == "original" else "")
                cal.add_component(event)

    return cal.to_ical()


if __name__ == "__main__":
    app.run()
